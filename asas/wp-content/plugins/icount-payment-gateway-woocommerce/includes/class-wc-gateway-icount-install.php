<?php

class WC_Gateway_iCount_Install {

	public static function init() {
		add_filter('plugin_action_links_'.IC_PLUGIN_BASENAME, Array(__CLASS__, 'plugin_action_links'));
		add_filter('plugin_row_meta', Array(__CLASS__, 'plugin_row_meta'), 10, 2);

		// Launch IPN forwarder
		ic_include_class("WC_Gateway_iCount_Forwarder");
		ic_include_class("WC_Gateway_iCount_PayPal_IPN_Forwarder");
		new WC_Gateway_iCount_PayPal_IPN_Forwarder();
	}

	/**
	 * Show action links on the plugin screen.
	 *
	 * @param	mixed $links Plugin Action links
	 * @return	array
	 */
	public static function plugin_action_links($links) {
		$action_links = array(
			'settings' => '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=icount' ) . '" title="' . esc_attr( __( 'iCount Gateway Settings', 'woocommerce' ) ) . '">' . __( 'Settings', 'woocommerce' ) . '</a>',
		);
		return array_merge( $action_links, $links );
	}

	public static function plugin_row_meta($links, $file) {
		if ( $file == IC_PLUGIN_BASENAME ) {
			$row_meta = array(
				'docs'    => '<a href="' . esc_url('http://www.icount.co.il/help/integration/woocommerce') . '" title="' . esc_attr(__('View iCount Documentation', 'woocommerce')) . '">' . __('Help', 'woocommerce') . '</a>',
				'support' => '<a href="' . esc_url('https://app.icount.co.il/support/?action=newissue') . '" title="' . esc_attr(__('Open Support Ticket', 'woocommerce')) . '">' . __('Support', 'woocommerce') . '</a>',
				'forum' => '<a href="' . esc_url('http://forum.icount.co.il/') . '" title="' . esc_attr(__('Visit Support Forum', 'woocommerce')) . '">' . __('Forum', 'woocommerce') . '</a>',
			);

			return array_merge( $links, $row_meta );
		}

		return (array) $links;
	}
}

?>
