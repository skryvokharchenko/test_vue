<?php
/*
Plugin Name: iCount Payment Gateway
Plugin URI: http://www.icount.co.il/
Description: Adds iCount Payment Gateway to WooCommerce
Version: 1.5.6
Author: iCount Systems
Author URI: http://www.icount.co.il/
*/

add_action('plugins_loaded', 'load_icount_gateway', 0);
function load_icount_gateway()
{
	if(!class_exists('WC_Payment_Gateway')) return;

	ic_defines();
	ic_includes();
	WC_Gateway_iCount_Install::init();

	add_filter('woocommerce_payment_gateways', 'add_icount_gateway');
}

function add_icount_gateway($methods)
{
	$methods[] = "WC_Gateway_iCount";
	return $methods;
}

function ic_defines()
{
	ic_define('IC_PLUGIN_FILE', __FILE__);
	ic_define('IC_PLUGIN_DIR', dirname(__FILE__));
	ic_define('IC_PLUGIN_INC', IC_PLUGIN_DIR.'/includes/');
	ic_define('IC_PLUGIN_BASENAME', plugin_basename(__FILE__));
	ic_define('IC_PLUGIN_URL', plugin_dir_url(__FILE__));
	ic_define('IC_PLUGIN_URL_IMG', IC_PLUGIN_URL."images/");
}

function ic_plugin_data()
{
	include_once(ABSPATH.'/wp-admin/includes/plugin.php');
	return get_plugin_data(__FILE__);
}

function ic_plugin_version()
{
	$data = ic_plugin_data();
	return $data["Version"];
}

function ic_define($name, $value)
{
	if(!defined($name)) define($name, $value);
}

function ic_includes()
{
	ic_include_class("WC_Gateway_iCount");
	ic_include_class("WC_Gateway_iCount_Install");
}

function ic_include_class($class)
{
	if(class_exists($class)) return true;

	$file = "class-".strtolower(str_replace("_","-",$class)).".php";

	ic_include_file($file);
	return class_exists($class);
}

function ic_include_file($file)
{
	$path = IC_PLUGIN_DIR."/includes/".$file;
	if(is_file($path)) include_once($path);
}

?>
