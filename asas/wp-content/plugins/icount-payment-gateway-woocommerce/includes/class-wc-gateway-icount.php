<?php
if(!defined('ABSPATH')) exit;

/**
 * iCount Payment Gateway
 *
 * Provides iCount Payment Gateway.
 *
 * @class 		WC_Gateway_iCount
 * @extends		WC_Payment_Gateway
 * @version		1.1
 * @package		iCount-Payment-Gateway-WooCommerce
 * @author 		iCount
 */
class WC_Gateway_iCount extends WC_Payment_Gateway
{
	/** @var boolean Whether or not logging is enabled */
	public static $log_enabled = false;

	/** @var WC_Logger Logger instance */
	public static $log = false;

	/**
	 * Constructor for the gateway.
	 */
	public function __construct() {
		ic_defines();
		ic_include_file("settings-icount.php");

		$this->id                 = 'icount';
		$this->has_fields         = false;
		$this->method_title       = __( 'iCount', 'woocommerce' );
		$this->method_description = sprintf( __( 'iCount Payment Gateway sends customers to iCount to enter their payment information. iCount IPN requires fsockopen/cURL support to update order statuses after payment. Check the %ssystem status%s page for more details.', 'woocommerce' ), '<a href="' . admin_url( 'admin.php?page=wc-status' ) . '">', '</a>' );

		$this->supports           = array(
			'products',
			//'refunds'
			);

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();

		// Define user set variables
		$this->title             = $this->get_option( 'title' );
		$this->description       = $this->get_option( 'description' );
		$this->order_button_text = $this->get_option('order_button_text');
		$this->debug             = 'yes' === $this->get_option( 'debug', 'no' );
		$this->token             = $this->get_option( 'token' );

		self::$log_enabled = $this->debug;

		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );


		if (!$this->is_valid_for_use()) {
			$this->enabled = 'no';
			return;
		}

		ic_include_class("WC_Gateway_iCount_IPN_Handler");
		new WC_Gateway_iCount_IPN_Handler();
	}

	/**
	 * Logging method
	 * @param  string $message
	 */
	public static function log( $message ) {
		if ( self::$log_enabled ) {
			if ( empty( self::$log ) ) {
				self::$log = new WC_Logger();
			}
			self::$log->add( 'icount', $message );
		}
	}

	public static function http_die($message="ERROR", $http_status_code=400, $http_status_text="Bad Request")
	{
		if(is_wp_error($message))
		{
			$error_code = $message->get_error_code();
			$error_message = $message->get_error_message();
			$error_data = $message->get_error_data();
			if(!empty($error_data["status"])) $http_status_code = $error_data["status"];
			if(!empty($error_data["status_text"])) $http_status_text = $error_data["status_text"];
			$message = $error_message;
		}

		self::log(($http_status_code==200?"":"ERROR: ").(is_scalar($message) ? $message : var_export($message,1)));
		header("HTTP/1.1 {$http_status_code} {$http_status_text}");
		die(is_scalar($message) ? $message : json_encode($message));
	}

	/**
	 * Calculate signature for request info
	 * @param Array $info
	 * @param string $secret
	 * @return string $signature
	 */
	public static function calculate_signature($info, $secret) {
		ksort($info);

		$message = '';
		foreach(array_keys($info) as $key)
		{
			if(strncmp($key,"wc_",3)) continue;
			if($key=="wc_signature") continue;
			$val = stripslashes($info[$key]);
			$message .= "\01".$key."\02".$val;
		}

		if(empty($message)) return false;

		self::log('message: '.$message);
		self::log('secret: '.$secret);

		$signature = hash_hmac('sha256', $message, $secret);
		self::log('signature: '.$signature);

		return $signature;
	}

	/**
	 * Get current site domain name
	 * @return string
	 */
	public static function get_site_domain() {
		return parse_url(get_site_url(), PHP_URL_HOST);
	}

	/**
	 * get_icon function.
	 * @return string
	 */
	public function get_icon() {
		$icon_html = '';
		$icon      = $this->get_icon_image();

		$icon_html .= '<img src="' . esc_attr( $icon ) . '" alt="' . esc_attr__( 'iCount', 'woocommerce' ) . '" />';

		//$icon_html .= sprintf( '<a href="%1$s" class="about_icount" target="_blank" style="float:right;">' . esc_attr__( 'What is iCount?', 'woocommerce' ) . '</a>', esc_url($this->get_icon_url()));

		return apply_filters( 'woocommerce_gateway_icon', $icon_html, $this->id );
	}

	/**
	 * Get the link for an icon based on country
	 * @return string
	 */
	protected function get_icon_url() {
		return 'http://www.icount.co.il/?utm_source='.self::get_site_domain().'&amp;utm_medium=wp&amp;utm_campaign=wooco_plugin&amp;utm_content=iconlink';
	}

	/**
	 * Get iCount image
	 * @return string image URL
	 */
	protected function get_icon_image() {
		$icon = WC_HTTPS::force_https_url(IC_PLUGIN_URL_IMG.'icount.png');
		return apply_filters('woocommerce_icount_icon', $icon);
	}

	/**
	 * Check if this gateway is enabled and available in the user's country
	 *
	 * @return bool
	 */
	public function is_valid_for_use() {
		return in_array( get_woocommerce_currency(), apply_filters( 'woocommerce_icount_supported_currencies', array( 'USD', 'EUR', 'ILS' ) ) );
	}

	/**
	 * Admin Panel Options
	 * - Options for bits like 'title' and availability on a country-by-country basis
	 *
	 * @since 1.0.0
	 */
	public function admin_options() {
		if ( $this->is_valid_for_use() ) {
			parent::admin_options();
		} else {
			?>
			<div class="inline error"><p><strong><?php _e( 'Gateway Disabled', 'woocommerce' ); ?></strong>: <?php _e( 'iCount does not support your store currency.', 'woocommerce' ); ?></p></div>
			<?php
		}
	}

	/**
	 * Initialise Gateway Settings Form Fields
	 */
	public function init_form_fields() {
		$this->form_fields = include( dirname(__FILE__).'/settings-icount.php' );
	}

	/**
	 * Get the transaction URL.
	 *
	 * @param  WC_Order $order
	 *
	 * @return string
	 */
	public function get_transaction_url( $order ) {
		$order_id = is_callable(Array($order, "get_id")) ? $order->get_id() : $order->id;
		return get_post_meta($order_id, "Receipt", true);
	}

	/**
	 * Process the payment and return the result
	 *
	 * @param int $order_id
	 * @return array
	 */
	public function process_payment( $order_id ) {
		ic_include_class("WC_Gateway_iCount_Request");

		$environment    = $this->get_option('environment');
		$order          = wc_get_order( $order_id );
		$icount_request = new WC_Gateway_iCount_Request( $this );

		$redirect_url	= $icount_request->get_request_url($order, $environment);
		if(empty($redirect_url)) return Array('result' => 'failure');

		return array(
			'result'   => 'success',
			'redirect' => $redirect_url,
			);
	}

	/**
	 * Can the order be refunded via iCount?
	 * @param  WC_Order $order
	 * @return bool
	 */
	public function can_refund_order( $order ) {
		$this->log('Refunds not supported');
		return false;
	}

	/**
	 * Process a refund if supported
	 * @param  int $order_id
	 * @param  float $amount
	 * @param  string $reason
	 * @return  boolean True or false based on success, or a WP_Error object
	 */
	public function process_refund( $order_id, $amount = null, $reason = '' ) {
		$this->log('Refunds not supported');
		return false;
	}
}
