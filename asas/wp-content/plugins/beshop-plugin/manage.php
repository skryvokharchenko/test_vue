<?php

/**
 * BeshopPlugin class
 * 
 * @since 0.0.1
 */
use ReallySimpleJWT\Token;

class BeshopPlugin {

    /**
     * Array of settings variables
     * 
     * @var array 
     */
    protected static $settings = array(
        'manage_page' => 'manage',
        'store_id' => '',
        'allowed_roles' => array('administrator'),
        'react_app_path' => __DIR__ . '/static/index.html',
        'token_secret' => ''
    );

    /**
     * Prepares the settings variables and registers the wp hook function.
     * 
     * @param array $options
     */
    function __construct($options = []) {
        $constants = [];

        if (defined('BESHOP_MANAGE_PAGE') && defined('BESHOP_STORE_ID') && defined('BESHOP_TOKEN_SECRET')) {
            $constants['manage_page'] = BESHOP_MANAGE_PAGE;
            $constants['store_id'] = BESHOP_STORE_ID;
            $constants['token_secret'] = BESHOP_TOKEN_SECRET;
            $constants['resto'] = defined('BESHOP_RESTO') ? BESHOP_RESTO : 1;
        } else {
            return;
        }

        //prepare plugin settings
        self::$settings = array_merge(self::$settings, $constants, $options);

        // hook wp
        add_action('wp', array('BeshopPlugin', 'managePage'));
    }

    /**
     * Will make checks and outputs contents of the `/static/index.html` file.
     * Redirects to login page if user role not allowed
     * 
     * @return string
     */
    public static function managePage() {
        if (!self::managePageExists() || !self::managePageIsCurrent()) {
            return;
        }
        if (!self::userAllowed()) {
            wp_redirect(wp_login_url());
            exit;
        }

        // Generate security token
        $token = self::createToken();

        // Write token to the cookies
        if (!isset($_COOKIE['beshop_token_v1'])) {
            setcookie('beshop_token_v1', $token, time() + 3600);
        }

        $react_app = file_get_contents(self::$settings['react_app_path']);

        echo $react_app;

        die();
    }

    /**
     * Will check the manage page exists
     * 
     * @return bool
     */
    protected static function managePageExists() {
        return get_page_by_path(self::$settings['manage_page'], OBJECT, 'page');
    }

    /**
     * Will check the current page is manage page
     * 
     * @return bool
     */
    protected static function managePageIsCurrent() {
        return is_page(self::$settings['manage_page']);
    }

    /**
     * Will check the current user role allowed to access
     * 
     * @return bool
     */
    protected static function userAllowed() {
        $user = wp_get_current_user();
        return array_intersect(self::$settings['allowed_roles'], $user->roles);
    }

    /**
     * Will generate JWT token using ReallySimpleJWT\Token with payload prop sid 
     * 
     * @return string
     */
    protected static function createToken() {

        $token = null;

        $payload = [
            'iat' => time(),
            'sid' => self::$settings['store_id'],
            'resto' => self::$settings['resto'],
            'exp' => time() + 3600,
            'iss' => 'localhost'
        ];

        try {
            $token = Token::customPayload($payload, self::$settings['token_secret']);
        } catch (Exception $ex) {
            error_log($ex->getMessage());
        }

        return $token;
    }

}
