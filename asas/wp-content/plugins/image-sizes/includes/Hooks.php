<?php
/**
 * All admin facing functions
 */

namespace codexpert\Image_Sizes;

/**
 * if accessed directly, exit.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @package Plugin
 * @subpackage Hooks
 * @author Nazmul Ahsan <n.mukto@gmail.com>
 */
class Hooks {

	public $plugin;

	/**
	 * Constructor function
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->slug = $this->plugin['TextDomain'];
		$this->name = $this->plugin['Name'];
		$this->version = $this->plugin['Version'];
	}
	
	/**
	 * @see register_activation_hook
	 * @uses codexpert\Image_Sizes\Admin
	 * @uses codexpert\Image_Sizes\Front
	 * @uses codexpert\Image_Sizes\API
	 */
	public function activate( $callback ) {
		register_activation_hook( CXIS, array( $this, $callback ) );
	}
	
	/**
	 * @see register_activation_hook
	 * @uses codexpert\Image_Sizes\Admin
	 * @uses codexpert\Image_Sizes\Front
	 * @uses codexpert\Image_Sizes\API
	 */
	public function deactivate( $callback ) {
		register_deactivation_hook( CXIS, array( $this, $callback ) );
	}
	
	/**
	 * @see add_action
	 * @uses codexpert\Image_Sizes\Admin
	 * @uses codexpert\Image_Sizes\Front
	 */
	public function action( $tag, $callback, $num_args = 1, $priority = 10 ) {
		add_action( $tag, array( $this, $callback ), $priority, $num_args );
	}

	/**
	 * @see add_filter
	 * @uses codexpert\Image_Sizes\Admin
	 * @uses codexpert\Image_Sizes\Front
	 */
	public function filter( $tag, $callback, $num_args = 1, $priority = 10 ) {
		add_filter( $tag, array( $this, $callback ), $priority, $num_args );
	}

	/**
	 * @see add_shortcode
	 * @uses codexpert\Image_Sizes\Shortcode
	 */
	public function register( $tag, $callback ) {
		add_shortcode( $tag, array( $this, $callback ) );
	}

	/**
	 * @see add_action( 'wp_ajax_..' )
	 * @uses codexpert\Image_Sizes\AJAX
	 */
	public function priv( $handle, $callback ) {
		$this->action( "wp_ajax_{$handle}", $callback );
	}

	/**
	 * @see add_action( 'wp_ajax_nopriv_..' )
	 * @uses codexpert\Image_Sizes\AJAX
	 */
	public function nopriv( $handle, $callback ) {
		$this->action( "wp_ajax_nopriv_{$handle}", $callback );
	}
}