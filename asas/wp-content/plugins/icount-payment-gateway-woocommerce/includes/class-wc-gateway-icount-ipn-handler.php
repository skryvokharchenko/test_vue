<?php
if(!defined('ABSPATH')) exit;

include_once( 'class-wc-gateway-icount-response.php' );

/**
 * Handles responses from iCount IPN
 */
class WC_Gateway_iCount_IPN_Handler extends WC_Gateway_iCount_Response {

	/** @var string Receiver email address to validate */
	protected $receiver_email;

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		add_action( 'woocommerce_api_wc_gateway_icount', array( $this, 'handle_ipn' ) );
	}

	/**
	 * Check for iCount IPN Response
	 */
	public function handle_ipn() {
		WC_Gateway_iCount::log(__METHOD__." init");
		WC_Gateway_iCount::log('Got IPN request: '.var_export($_REQUEST,1));

		$posted = wp_unslash( $_REQUEST );
		$order = $this->validate_request($posted);
		
		if(is_wp_error($order)) WC_Gateway_iCount::http_die($order);

		$order_id = is_callable(Array($order, "get_id")) ? $order->get_id() : $order->id;
		WC_Gateway_iCount::log('Found order #' . $order_id);

		$this->payment_status_completed($order, $posted);

		WC_Gateway_iCount::log(__METHOD__." DONE");
	}

	/**
	 * Check that the request has document information (receipt issued)
	 * @param WC_Order $order
	 * @param Array $posted
	 */
	protected function validate_doc($order, $posted) {
		WC_Gateway_iCount::log(__METHOD__." init");
		if(empty($posted["doctype"]) || empty($posted["docnum"]))
		{
			$this->payment_on_hold( $order, sprintf( __( 'Payment pending: %s', 'woocommerce' ), $posted['pending_reason'] ) );
			WC_Gateway_iCount::http_die("Payment on hold: Missing doctype/docnum", 405, "Payment Required");
		}
		WC_Gateway_iCount::log(__METHOD__." done");
	}

	/**
	 * Check currency from IPN matches the order
	 * @param  WC_Order $order
	 * @param  string $currency
	 */
	protected function validate_currency( $order, $currency ) {
		WC_Gateway_iCount::log(__METHOD__." init");
		// Validate currency
		if ( $order->get_order_currency() != $currency ) {
			WC_Gateway_iCount::log( 'Payment error: Currencies do not match (sent "' . $order->get_order_currency() . '" | returned "' . $currency . '")' );

			// Put this order on-hold for manual checking
			$order->update_status( 'on-hold', sprintf( __( 'Validation error: iCount currencies do not match (code %s).', 'woocommerce' ), $currency ) );
			WC_Gateway_iCount::http_die("Payment on hold: Currency mismatch", 409, "Conflict");
		}
		WC_Gateway_iCount::log(__METHOD__." done");
	}

	/**
	 * Check payment amount from IPN matches the order
	 * @param  WC_Order $order
	 */
	protected function validate_amount( $order, $amount ) {
		WC_Gateway_iCount::log(__METHOD__." init");
		if ( number_format( $order->get_total(), 2, '.', '' ) != number_format( $amount, 2, '.', '' ) ) {
			WC_Gateway_iCount::log( 'Payment error: Amounts do not match (gross ' . $amount . ')' );

			// Put this order on-hold for manual checking
			$order->update_status( 'on-hold', sprintf( __( 'Validation error: iCount amounts do not match (gross %s).', 'woocommerce' ), $amount ) );
			WC_Gateway_iCount::http_die("Payment on hold: Payment amount mismatch", 412, "Precondition Failed");
		}
		WC_Gateway_iCount::log(__METHOD__." done");
	}

	/**
	 * Handle a completed payment
	 * @param  WC_Order $order
	 */
	protected function payment_status_completed( $order, $posted ) {
		WC_Gateway_iCount::log(__METHOD__." init");
		$order_id = is_callable(Array($order, "get_id")) ? $order->get_id() : $order->id;

		if ( $order->has_status( 'completed' ) ) {
			WC_Gateway_iCount::http_die('Aborting, Order #' . $order_id . ' is already complete.', 200, "OK");
		}

		$this->validate_doc($order, $posted);
		$this->validate_currency( $order, $posted['currency_code'] );
		$this->validate_amount( $order, $posted['sum'] );
		$this->save_icount_meta_data( $order, $posted );

		$txn_id = $posted["doctype"]."#".$posted["docnum"];
		WC_Gateway_iCount::log(__METHOD__." txn_id: ".$txn_id);

		$notes = __("Payment completed", "woocommerce");

		if(!empty($posted["cc_last4"]))
		{
			$cc_info = $posted["cc_type"]." ".$posted["cc_last4"];
			WC_Gateway_iCount::log(__METHOD__." cc_info: ".$cc_info);
			$notes .= "<br>".__("Paid with","woocommerce").": ".$cc_info;
		}

		if(!empty($posted["num_of_payments"]) && $posted["num_of_payments"]>1)
		{
			WC_Gateway_iCount::log(__METHOD__." num_of_payments: ".$posted["num_of_payments"]);
			$notes .= "<br>".__("Installments","woocommerce").": ".$posted["num_of_payments"];

			if(!empty($posted["total_interest"]))
			{
				WC_Gateway_iCount::log(__METHOD__." interest({$posted["interest_rate"]}%): ".$posted["total_interest"]);
				$notes .= "<br>".__("Interest rate","woocommerce").": ".$posted["interest_rate"]."%";
				$notes .= "<br>".__("Total interest","woocommerce").": ".$posted["total_interest"];
				$notes .= "<br>".__("Total paid","woocommerce").": ".$posted["total_paid"];
			}
		}

		if(!empty($posted["confirmation_code"]))
		{
			WC_Gateway_iCount::log(__METHOD__." confirmation_code: ".$posted["confirmation_code"]);
			$notes .= "<br>".__("Confirmation code", "woocommerce").": ".$posted["confirmation_code"];
		}

		$this->payment_complete( $order, $txn_id, $notes);
		WC_Gateway_iCount::log(__METHOD__." done");
	}

	/**
	 * Save important data from the IPN to the order
	 * @param WC_Order $order
	 */
	protected function save_icount_meta_data( $order, $posted ) {
		WC_Gateway_iCount::log(__METHOD__." init");
		$order_id = is_callable(Array($order, "get_id")) ? $order->get_id() : $order->id;

		if ( ! empty( $posted['customer_email'] ) ) {
			update_post_meta( $order_id, 'Payer email address', wc_clean( $posted['customer_email'] ) );
		}
		if ( ! empty( $posted['customer_phone'] ) ) {
			update_post_meta( $order_id, 'Payer phone', wc_clean( $posted['customer_phone'] ) );
		}
		if ( ! empty( $posted['customer_fname'] ) ) {
			update_post_meta( $order_id, 'Payer first name', wc_clean( $posted['customer_fname'] ) );
		}
		if ( ! empty( $posted['customer_lname'] ) ) {
			update_post_meta( $order_id, 'Payer last name', wc_clean( $posted['customer_lname'] ) );
		}
		if ( ! empty( $posted['num_of_payments'] ) ) {
			update_post_meta( $order_id, 'Number of installments', wc_clean( $posted['num_of_payments'] ) );
		}
		if ( ! empty( $posted['confirmation_code'] ) ) {
			update_post_meta( $order_id, 'Confirmation code', wc_clean( $posted['confirmation_code'] ) );
		}
		if(!empty($posted["doc_url"])) {
			update_post_meta( $order_id, "Receipt", $posted["doc_url"]);
		}
		if(!empty($posted["cc_last4"]))
		{
			$cc_info = $posted["cc_type"]." ".$posted["cc_last4"];
			WC_Gateway_iCount::log(__METHOD__." cc_info: ".$cc_info);

			if(is_callable(Array($order, "set_payment_method_title")))
				$order->set_payment_method_title($cc_info);
			else
				update_post_meta( $order_id, "_payment_method_title", $cc_info);
		}

		// interest
		if(!empty($posted["total_interest"]))
		{
			update_post_meta($order_id, "Interest rate", $posted["interest_rate"]);
			update_post_meta($order_id, "Interest amount", $posted["total_interest"]);
		}

		WC_Gateway_iCount::log(__METHOD__." done");
	}
}
