<?php

class Backend extends BeshopPlugin {

    private $token;
    private $storeid;
    private $backend_base_url;

    public function __construct() {
        parent::__construct();

        $this->token = $this->createToken();
        $this->storeid = self::$settings['store_id'];
        $this->backend_base_url = defined('BESHOP_BACKEND_URL') ? BESHOP_BACKEND_URL : 'https://backshop.beonline.studio/';

//        add_action('woocommerce_update_product', array($this, 'product_was_updated'), 10, 1);
//        add_action('woocommerce_new_product', array($this, 'product_was_updated'), 10, 1);
    }

    public function product_was_updated($product_id) {
        $product = wc_get_product($product_id);

        $image_id = $product->get_image_id();
        $image_url = wp_get_attachment_image_url($image_id, 'full');

        $categories = wp_get_post_terms($product_id, 'product_cat');
        $category = reset($categories);
        $woo_category_name = $category->name;
        $backend_category = $this->get_backend_category($woo_category_name);

        $payload = array(
            'barcode' => $product->get_sku(),
            'category' => $backend_category->id,
            'description' => $product->get_description(),
            'image' => $image_url,
            'owner' => BESHOP_STORE_ID,
            'price' => $product->get_price(),
            'title' => $product->get_name(),
            'weight' => $product->get_weight()
        );

        $result = $this->callBackend('product/create', 'POST', $payload);
    }

    protected function get_backend_category($cat_name) {

        $response = $this->callBackend('category?by_name=' . $cat_name, 'GET');

        $response_data = json_decode($response['body']);
        return $response_data->data;
    }

    protected function callBackend($path, $method, $payload = false) {
        try {
            $body = array();
            if ($payload) {
                $body = array(
                    'body' => json_encode($payload, JSON_FORCE_OBJECT),
                    'data_format' => 'body'
                );
            }
            $response = wp_remote_request($this->backend_base_url . $path, array_merge(
                            array(
                'method' => $method,
                'headers' => array(
                    'Authorization' => $this->token,
                    'Content-Type' => 'application/json; charset=utf-8'
                ),
                            ), $body));
        } catch (Exception $e) {
            error_log('BESHOP PLUGIN ERROR. Call to backend error. Message: ' . $e->getMessage());
        }

        return $response;
    }

}
