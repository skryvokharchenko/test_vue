<?php
if(!defined('ABSPATH')) exit;

class WC_Gateway_iCount_PayPal_IPN_Forwarder extends WC_Gateway_iCount_Forwarder {

	public function __construct() {
		parent::__construct();
		add_action( 'valid-paypal-standard-ipn-request', array( $this, 'forward_ipn' ), 0 ); // For "PayPal Standard" gateway
		add_action( 'woocommerce_paypal_express_checkout_valid_ipn_request', array( $this, 'forward_ipn' ), 0 ); // For "Paypal Express Checkout" gateway
	}

	public function forward_ipn()
	{
		if('yes' !== $this->get_option('paypal_ipn_forward_enabled', 'no'))
		{
			WC_Gateway_iCount::log("iCount PayPal IPN Forwarder is OFF");
			return;
		}
		
		$url = $this->get_option('paypal_ipn_forward_url');
		if(empty($url))
		{
			WC_Gateway_iCount::log("iCount PayPal IPN Forwarder URL is empty!");
			return;
		}

		parent::forward_request($url);
	}
}
