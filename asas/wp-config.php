<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'asas' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
define('FS_METHOD','direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0DKiKgo@Yw>:+S]Hb80xToe<Yv?GA)#VWLcrB/8kEix*LR.~Hf+ySdc1AnYjT1Yq' );
define( 'SECURE_AUTH_KEY',  'cD4}?0S#gZSYoQ$[BM=HII&%Y%BmtC6 Brro1H[1V?KNB&dRP;Sxka1[rzm<%}}Z' );
define( 'LOGGED_IN_KEY',    'm&.lg4XAKAqo[t])x{W#G}t*C)73$:^*[B>$|/b5CCRPBu9]@LD* M?KyUm2Ah&^' );
define( 'NONCE_KEY',        'nI 4``&$(+?JDsCFW)[Pj>Q!dZ-P]],OJ4Cu)j^ssUp[gZw;J={;-MsoU`?,LI/5' );
define( 'AUTH_SALT',        'mR!|Zy6Os{G@D2aC_$SQ8)*wP,[3zgcJL7p@B,apcuqQ^Yz=I9,6Q87fKYg`:bB6' );
define( 'SECURE_AUTH_SALT', '5}0we%zw^Y2KvypT#-VaNm#a$^GaC(EUmecU*$4gK^njDk14g&a:|;-{kk*{u.IB' );
define( 'LOGGED_IN_SALT',   's7x?!,0JV[P*z4`S4 B[t_B~,sp]//_E:g8r9^|dB`Le{O2]tnO2Q`g4W!W%X)7+' );
define( 'NONCE_SALT',       '>8dXVEi70RF8g}ukUo,N)nHOySTv!*i{j<*!p$-Mpx.[$=/X-Zu0w^<J0n9hK!GL' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
