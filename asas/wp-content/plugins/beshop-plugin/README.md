# Beshop Plugin
Plugin that extends WP with beshop functionality

## Description
The plugin checks if the `/manage` is exists and current user has permissions to access it, then outputs contents of the `/static/index.html` file.
Redirects a user to the login page if the current user's role not allowed to access. Writes security token to the cookies. Cookie name is `beshop_token`.

## Usage
1. Install and active plugin in the original WP way.
2. Update dependencies. Run `composer update` in the plugin directory.
3. Add constant settings to `wp-config.php`
    define('BESHOP_MANAGE_PAGE', 'manage');
    define('BESHOP_STORE_ID', 123);
    define('BESHOP_TOKEN_SECRET', 'some-secret-string');
    define('BESHOP_PRESISTENT_SALT', 'RO={3gzn:mi|.hew0:wl8]$2p3wlQ=op+V.%AXCZcn-#nux(fStG.#nf%E -ovBr');
    define('BESHOP_RESTO', 0);
    define('BESHOP_BACKEND_URL', 'http://beonline.local/');
4. Create empty page with slug `manage`

## Configuration
All settings are stored at top of the `manage.php` file.

