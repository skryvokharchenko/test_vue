<?php
/**
 * All admin facing functions
 */

namespace codexpert\Image_Sizes;

/**
 * if accessed directly, exit.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @package Plugin
 * @subpackage Admin
 * @author Nazmul Ahsan <n.mukto@gmail.com>
 */
class Admin extends Hooks {

	public $plugin;

	/**
	 * Constructor function
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->slug = $this->plugin['TextDomain'];
		$this->name = $this->plugin['Name'];
		$this->version = $this->plugin['Version'];
	}

	/**
	 * Installer. Runs once when the plugin in astivated.
	 *
	 * @since 1.0
	 */
	public function install() {
		/**
		 * Schedule an event to sync help docs
		 */
		if ( !wp_next_scheduled ( 'image-sizes_daily' )) {
		    wp_schedule_event( time(), 'daily', 'image-sizes_daily' );
		}

		/**
		 * Sync docs
		 */
		self::sync_docs();
	}

	/**
	 * If the plugin is re-activated
	 */
	public function reask_survey() {
		if( get_option( 'image-sizes_survey_agreed' ) != 1 ) {
			delete_option( 'image-sizes_survey' );
		}
	}

	public function set_sizes() {
		update_option( '_image-sizes', cxis_get_image_sizes() );
	}
	
	/**
	 * Enqueue JavaScripts and stylesheets
	 */
	public function enqueue_scripts() {
		$min = defined( 'CXIS_DEBUG' ) && CXIS_DEBUG ? '' : '.min';
		
		wp_enqueue_script( $this->slug, plugins_url( "/assets/js/admin{$min}.js", CXIS ), [ 'jquery' ], $this->version, true );
		wp_enqueue_style( $this->slug, plugins_url( "/assets/css/admin{$min}.css", CXIS ), '', $this->version, 'all' );
		
		$localized = array(
			'ajaxurl'	=> admin_url( 'admin-ajax.php' ),
			'nonce'		=> wp_create_nonce( $this->slug ),
			'regen'		=> __( 'Regenerate', 'image-sizes' ),
			'regening'	=> __( 'Regenerating..', 'image-sizes' ),
		);
		wp_localize_script( $this->slug, 'CXIS', apply_filters( "{$this->slug}-localized", $localized ) );
	}

	/**
     * unset image size(s)
     *
     * @since 1.0
     */
    public function image_sizes( $sizes ){
        $disables = cx_get_option( 'prevent_image_sizes', 'disables', [] );

        if( count( $disables ) ) :
        foreach( $disables as $disable ){
            unset( $sizes[ $disable ] );
        }
        endif;
        
        return $sizes;
    }

	/**
	 * Sync docs from https://help.codexpert.io daily
	 *
	 * @since 1.0
	 */
	public static function sync_docs() {
	    $json_url = 'https://help.codexpert.io/wp-json/wp/v2/docs/?parent=1375&per_page=20';
	    if( !is_wp_error( $data = wp_remote_get( $json_url ) ) ) {
	        update_option( 'image-sizes-docs-json', json_decode( $data['body'], true ) );
	    }
	}
}