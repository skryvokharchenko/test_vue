<?php
/**
 * All settings facing functions
 */

namespace codexpert\Image_Sizes;

/**
 * @package Plugin
 * @subpackage Settings
 * @author Nazmul Ahsan <n.mukto@gmail.com>
 */
class Settings extends Hooks {

	public $plugin;

	/**
	 * Constructor function
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->slug = $this->plugin['TextDomain'];
		$this->name = $this->plugin['Name'];
		$this->version = $this->plugin['Version'];
	}

	public function add_admin_bar( $admin_bar ) {
		if( is_admin() ) return;

		$admin_bar->add_menu( array(
			'id'    => $this->slug,
			'title' => __( 'Image Sizes', 'image-sizes' ),
			'href'  => add_query_arg( 'page', $this->slug, admin_url( 'admin.php' ) ),
			'meta'  => array(
				'title' => $this->name,            
			),
		) );
	}

    /**
     * @since 2.0.2
     *
     */
    public function admin_notices() {

        if( !current_user_can( 'manage_options' ) || get_option( 'image-sizes_regened' ) == 1 ) return;

        if( isset( $_GET['page'] ) && $_GET['page'] == 'image-sizes' ) {
            update_option( 'image-sizes_regened', 1 );
            update_option( 'image-sizes_version', 3.0 );
            return;
        }

        $version_updated = get_option( 'image-sizes_configured' ) != '';

        ?>
        <div class="notice notice-success cxis-notice">
        	<?php if( $version_updated ) echo '<i class="notice-dismiss cxis-dismiss" data-meta_key="image-sizes_regened"></i>'; ?>
            <?php echo '<h3>' . sprintf( __( 'Hello %s!', 'image-sizes' ), wp_get_current_user()->display_name ) . '</h3>'; ?>
            <div>
                <?php
                if( $version_updated ) {
                	echo '<p>' . sprintf( __( 'It looks like you already have disabled some thumbnails with the <strong>%s</strong> plugin. Congratulations!', 'image-sizes' ), 'Image Sizes'  ) . '</p>';
                	echo '<p>' . __( 'Do you know, you can now regenerate thumbnaiils of your existing images?', 'image-sizes' ) . '</p>';
                }
                else {
                	echo '<p>' . sprintf( __( 'Thank you for taking your decision to install the <strong>%s</strong> plugin. Congratulations!', 'image-sizes' ), 'Image Sizes' ) . '</p>';
                	echo '<p>' . sprintf( __( 'You can now prevent WordPress from generating unnecessary thumbnails when you upload an image. You just need to select the thumbnail sizes from the settings screen. And.. relax!', 'image-sizes' ), 'Image Sizes' ) . '</p>';
                }
                ?>
            </div>
            <a class="cx-notice-btn" href="<?php echo admin_url( 'admin.php?page=image-sizes' ); ?>">
            	<?php echo $version_updated ? __( 'Click Here To Regenerate Thumbnails', 'image-sizes' ) : __( 'Click Here To Disable Thumbnails', 'image-sizes' ); ?>
            </a>
        </div>
        <?php
    }
	
	public function init_menu() {

		$image_sizes = get_option( '_image-sizes' );
		$settings = [
			'id'            => $this->slug,
			'label'         => __( 'Image Sizes', 'image-sizes' ),
			'title'         => $this->name,
			'header'        => $this->name,
			'priority'      => 10,
			'capability'    => 'manage_options',
			'icon'          => 'dashicons-image-crop',
			'position'      => '10.5',
			'sections'      => [
				'prevent_image_sizes'	=> 	[
					'id'        => 'prevent_image_sizes',
					'label'     => __( 'Disable Sizes', 'image-sizes' ),
					'icon'      => 'dashicons-images-alt2',
					'color'		=> '#4c3f93',
					'sticky'	=> true,
					'template'	=> cxis_get_template( 'disable-sizes', 'templates/settings', [ 'image_sizes' => $image_sizes ] ),
					'fields'    => []
				],
				'image-sizes_regenerate'	=> 	[
					'id'        => 'image-sizes_regenerate',
					'label'     => __( 'Regenerate Thumbnails', 'image-sizes' ),
					'icon'      => 'dashicons-format-gallery',
					'color'		=> '#ff0993',
					'hide_form'	=> true,
					'template'	=> cxis_get_template( 'regenerate-thumbnails', 'templates/settings' ),
					'fields'    => []
				],
				'image-sizes_help'	=> [
					'id'        => 'image-sizes_help',
					'label'     => __( 'Help', 'image-sizes' ),
					'icon'      => 'dashicons-buddicons-groups',
					'color'		=> '#34B6E9',
					'hide_form'	=> true,
					'template'	=> cxis_get_template( 'help', 'templates/settings' ),
					'fields'    => [],
				],
			],
		];

		new \CX_Settings_API( $settings );
	}

	public function sidebar( $config ) {
		if( $this->slug != $config['id'] ) return;

		$plugins	= get_plugins();
		$has_wc		= array_key_exists( 'woocommerce/woocommerce.php', $plugins );
		$has_el		= array_key_exists( 'elementor/elementor.php', $plugins );

		$share_logins = "<a href='http://bit.ly/sl-is' target='_blank'><img src='" . plugins_url( 'assets/img/share-logins.png', CXIS ) . "' />";
		$woolementor = "<a href='http://bit.ly/wl-is' target='_blank'><img src='" . plugins_url( 'assets/img/woolementor.png', CXIS ) . "' />";

		echo $share_logins;
		echo $woolementor;

		// if( $has_wc && $has_el ) {
		// 	echo $woolementor;
		// }
		// elseif( $has_wc && !$has_el ) {
		// 	echo $woolementor;
		// }
		// else {
		// 	echo $share_logins;
		// }
	}
}