<?php
/*
Plugin Name: Stop Generating Unnecessary Thumbnails
Description: So, it creates multiple thumbnails while uploading an image? Here is the solution!
Plugin URI: https://codexpert.io
Author: codexpert
Author URI: https://codexpert.io
Version: 3.0.4
Text Domain: image-sizes
Domain Path: /languages
*/

namespace codexpert\Image_Sizes;
use codexpert\Product\Survey;

/**
 * if accessed directly, exit.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'CXIS', __FILE__ );
define( 'CXIS_DEBUG', false );

/**
 * Main class for the plugin
 * @package Plugin
 * @author Nazmul Ahsan <n.mukto@gmail.com>
 */
class Plugin {
	
	public static $_instance;
	public $slug;
	public $name;
	public $version;
	public $server;
	public $required_php = '5.6';
	public $required_wp = '4.0';

	public function __construct() {
		self::define();
		
		if( !$this->_compatible() ) return;

		self::includes();
		self::hooks();
	}

	/**
	 * Define constants
	 */
	public function define(){
		if( !function_exists( 'get_plugin_data' ) ) {
		    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}
		$this->plugin = get_plugin_data( CXIS );

		$this->server = 'https://my.codexpert.io';

		$this->plugin['File'] = CXIS;
		$this->plugin['Server'] = $this->server;
	}

	/**
	 * Compatibility and dependency
	 */
	public function _compatible() {
		$_compatible = true;

		if( !file_exists( dirname( CXIS ) . '/vendor/autoload.php' ) ) {
			add_action( 'admin_notices', function() {
				echo "
					<div class='notice notice-error'>
						<p>" . sprintf( __( 'Packages are not installed. Please run <code>%s</code> in <code>%s</code> directory!', 'pointpress-wc' ), 'composer update', dirname( CXIS ) ) . "</p>
					</div>
				";
			} );

			$_compatible = false;
		}

		if( version_compare( get_bloginfo( 'version' ), $this->required_wp, '<' ) ) {
			add_action( 'admin_notices', function() {
				echo "
					<div class='notice notice-error'>
						<p>" . sprintf( __( '<strong>%s</strong> requires <i>WordPress version %s</i> or higher. You have <i>version %s</i> installed.', 'image-sizes' ), $this->name, $this->required_wp, get_bloginfo( 'version' ) ) . "</p>
					</div>
				";
			} );

			$_compatible = false;
		}

		if( version_compare( PHP_VERSION, $this->required_php, '<' ) ) {
			add_action( 'admin_notices', function() {
				echo "
					<div class='notice notice-error'>
						<p>" . sprintf( __( '<strong>%s</strong> requires <i>PHP version %s</i> or higher. You have <i>version %s</i> installed.', 'image-sizes' ), $this->name, $this->required_php, PHP_VERSION ) . "</p>
					</div>
				";
			} );

			$_compatible = false;
		}

		return $_compatible;
	}

	/**
	 * Includes files
	 */
	public function includes(){
		require_once dirname( CXIS ) . '/vendor/autoload.php';
		require_once dirname( CXIS ) . '/includes/functions.php';
	}

	/**
	 * Hooks
	 */
	public function hooks(){
		// i18n
		add_action( 'plugins_loaded', array( $this, 'i18n' ) );

		/**
		 * Admin facing hooks
		 *
		 * To add an action, use $admin->action()
		 * To apply a filter, use $admin->filter()
		 */
		$admin = new Admin( $this->plugin );
		$admin->activate( 'install' );
		$admin->activate( 'sync_docs' );
		$admin->activate( 'reask_survey' );
		$admin->action( 'wp_head', 'set_sizes' );
		$admin->action( 'admin_head', 'set_sizes' );
		$admin->action( 'admin_enqueue_scripts', 'enqueue_scripts' );
		$admin->filter( 'intermediate_image_sizes_advanced', 'image_sizes' );
		$admin->action( 'image-sizes_daily', 'sync_docs' );

		/**
		 * Settings related hooks
		 *
		 * To add an action, use $settings->action()
		 * To apply a filter, use $settings->filter()
		 */
		$settings = new Settings( $this->plugin );
		$settings->action( 'init', 'init_menu' );
		$settings->action( 'admin_bar_menu', 'add_admin_bar', 1, 70 );
		$settings->action( 'admin_notices', 'admin_notices' );
		$settings->action( 'cx-settings-sidebar', 'sidebar' );

		/**
		 * AJAX facing hooks
		 *
		 * To add a hook for logged in users, use $ajax->priv()
		 * To add a hook for non-logged in users, use $ajax->nopriv()
		 */
		$ajax = new AJAX( $this->plugin );
		$ajax->priv( 'cxis-regen-thumbs', 'regen_thumbs' );
		$ajax->priv( 'cxis-dismiss', 'dismiss_notice' );

		// Product related classes
		$survey = new Survey( $this->plugin );
	}

	/**
	 * Internationalization
	 */
	public function i18n() {
		load_plugin_textdomain( 'image-sizes', false, dirname( plugin_basename( CXIS ) ) . '/languages/' );
	}

	/**
	 * Cloning is forbidden.
	 */
	private function __clone() { }

	/**
	 * Unserializing instances of this class is forbidden.
	 */
	private function __wakeup() { }

	/**
	 * Instantiate the plugin
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
}

Plugin::instance();