<?php
if(!defined('ABSPATH')) exit;

/**
 * Generates requests to send to iCount
 */
class WC_Gateway_iCount_Request {

	/**
	 * Stores line items to send to iCount
	 * @var array
	 */
	protected $line_items = array();

	/**
	 * Pointer to gateway making the request
	 * @var WC_Gateway_iCount
	 */
	protected $gateway;

	/**
	 * Endpoint for requests from iCount
	 * @var string
	 */
	protected $notify_url;

	/**
	 * Constructor
	 * @param WC_Gateway_iCount $gateway
	 */
	public function __construct( $gateway ) {
		$this->gateway    = $gateway;
		$this->notify_url = WC()->api_request_url( 'WC_Gateway_iCount' );
	}

	/**
	 * Get the iCount request URL for an order
	 * @param  WC_Order  $order
	 * @param  string $environment
	 * @return string
	 */
	public function get_request_url( $order, $environment=null ) {
		if(empty($environment)) $environment = $this->gateway->get_option('environment');

		$url = "https://{$environment}.icount.co.il/woocommerce/pay.php";
		$args = $this->get_icount_args( $order );

		WC_Gateway_iCount::log("get_request_url: Sending POST request with cart data to iCount");
		WC_Gateway_iCount::log("get_request_url: URL: ".$url);
		WC_Gateway_iCount::log("get_request_url: params: ".var_export($args,1));

		$response = wp_remote_post($url, Array(
			"body" => $args,
			"headers" => Array("referer" => get_site_url()),
			"timeout" => 60,
			));
		if(is_wp_error($response))
		{
			WC_Gateway_iCount::log("get_request_url: WP_Error: ".implode("; ", $response->get_error_messages()));
			return false;
		}

		WC_Gateway_iCount::log("get_request_url: HTTP Status: ".$response["response"]["code"]." ".$response["response"]["message"]);
		if($response["response"]["code"]!=200)
		{
			WC_Gateway_iCount::log("get_request_url: ERROR: Bad HTTP status");
			return false;
		}
	
		if(empty($response["body"]))
		{
			WC_Gateway_iCount::log("get_request_url: ERROR: Empty response body");
			return false;
		}

		WC_Gateway_iCount::log("get_request_url: Response body: ".$response["body"]);
		if(!$json=json_decode($response["body"]))
		{
			WC_Gateway_iCount::log("get_request_url: ERROR: Response is not JSON");
		}
		elseif(!is_object($json))
		{
			WC_Gateway_iCount::log("get_request_url: ERROR: Response is not JSON object");
		}
		elseif(empty($json->status) || empty($json->sid) || empty($json->un))
		{
			$error_reason = !empty($json->error) ? $json->error : "Unknown error";
			WC_Gateway_iCount::log("get_request_url: ERROR: Request failed! Reason: ".$error_reason);
		}
		else
		{
			$args = Array(
				"token" => $args["token"],
				"sid" => $json->sid,
				"un" => $json->un
				);
			return $url."?".http_build_query($args, null, '&');
		}

		return false;
	}

	protected function __get_order_attr($order, $attr) {
		$func = "get_".$attr;
		return is_callable(Array($order, $func)) ? call_user_func(Array($order, $func)) : $order->$attr;
	}

	protected function __get_order_address($order, $address_type="billing")
	{
		$addr = Array();

		foreach(Array("first_name","last_name","company","email","phone","address_1","address_2","city","state","zip","country") as $attr)
		{
			switch($attr)
			{
				case "state":
					// preproces state
					$addr[$attr] = $this->get_icount_state(
						$this->__get_order_attr($order, $address_type."_country"),
						$this->__get_order_attr($order, $address_type."_state")
						);
					break;
				case "zip":
					$addr[$attr] = $this->__get_order_attr($order, $address_type."_postcode");
					break;
				default:
					$addr[$attr] = $this->__get_order_attr($order, $address_type."_".$attr);
					break;
			}
		}

		return $addr;
	}

	/**
	 * Get iCount Args for passing to PP
	 *
	 * @param WC_Order $order
	 * @return array
	 */
	protected function get_icount_args( $order ) {
		WC_Gateway_iCount::log( 'Generating payment form for order ' . $order->get_order_number() . '. Notify URL: ' . $this->notify_url );

		$order_id = $this->__get_order_attr($order, "id");
		$order_key = $this->__get_order_attr($order, "order_key");

		$billing_addr = $this->__get_order_address($order, "billing");
		WC_Gateway_iCount::log("Billing Address: ".var_export($billing_addr,1));

		$args = Array(
			// plugin info
			'wp_version'      => get_bloginfo('version'),
			'wc_version'      => WC_VERSION,
			'ic_version'      => ic_plugin_version(),

			// access token
			'token'           => $this->gateway->get_option('token'),

			// URLs
			'success_url'     => esc_url_raw( add_query_arg( 'utm_nooverride', '1', $this->gateway->get_return_url( $order ) ) ),
			'failure_url'     => esc_url_raw( $order->get_cancel_order_url() ),
			'cancel_url'      => esc_url_raw( $order->get_cancel_order_url() ),
			'ipn_url'         => $this->notify_url,

			// order reference
			'wc_order_id'     => $order_id,
			'wc_order_key'    => $order_key,
			'wc_order_number' => $order->get_order_number(),

			// order totals
			'cs'              => $order->get_total(),
			'cd'              => $this->get_order_item_names($order),
			'currency_code'   => get_woocommerce_currency(),

			// buyer information
			'ccfname'         => $billing_addr["first_name"],
			'cclname'         => $billing_addr["last_name"],
			'first_name'      => $billing_addr["first_name"],
			'last_name'       => $billing_addr["last_name"],
			'full_name'       => $billing_addr["company"] ?: trim($billing_addr["first_name"]." ".$billing_addr["last_name"]),
			'contact_email'   => $billing_addr["email"],
			'contact_phone'   => $billing_addr["phone"],
			'address1'        => $billing_addr["address_1"],
			'address2'        => $billing_addr["address_2"],
			'city'            => $billing_addr["city"],
			'state'           => $billing_addr["state"],
			'zip'             => $billing_addr["zip"],
			'country'         => $billing_addr["country"],

			// is this a test transaction? (no billing will be performed, but the document will be issued)
			'x_test'          => $this->gateway->get_option('testmode')==='yes'?1:0,

			// forward website language
			'lang'            => substr(get_locale(),0,2),
			);

		return apply_filters( 'woocommerce_icount_args', array_merge(
			$args,
			$this->get_shipping_args( $order ),
			$this->get_line_item_args( $order )
			), $order );
	}

	/**
	 * Get shipping args for icount request
	 * @param  WC_Order $order
	 * @return array
	 */
	protected function get_shipping_args( $order ) {
		$shipping_args = array();

		$shipping_method = $order->get_shipping_method();
		if(!empty($shipping_method))
		{
			$shipping_addr = $this->__get_order_address($order, "shipping");
			WC_Gateway_iCount::log("Shipping Method: ".$shipping_method);
			WC_Gateway_iCount::log("Shipping Address: ".var_export($shipping_addr,1));

			// If we are sending shipping, send shipping address instead of billing
			$shipping_args = Array(
				'delivery_method'     => $shipping_method,
				'shipping_first_name' => $shipping_addr["first_name"],
				'shipping_last_name'  => $shipping_addr["last_name"],
				'shipping_full_name'  => $shipping_addr["company"] ?: trim($shipping_addr["first_name"]." ".$shipping_addr["last_name"]),
				'shipping_address1'   => $shipping_addr["address_1"],
				'shipping_address2'   => $shipping_addr["address_2"],
				'shipping_city'       => $shipping_addr["city"],
				'shipping_state'      => $shipping_addr["state"],
				'shipping_zip'        => $shipping_addr["zip"],
				'shipping_country'    => $shipping_addr["country"],
				);
		}

		return $shipping_args;
	}

	/**
	 * Get line item args for icount request
	 * @param  WC_Order $order
	 * @return array
	 */
	protected function get_line_item_args( $order ) {

		$line_item_args = Array();

		$this->prepare_line_items($order);
		$line_item_args['items'] = $this->get_line_items();

		if(wc_tax_enabled())
		{
			WC_Gateway_iCount::log("Taxes enabled!");
			$line_item_args['totalwithvat'] = $order->get_total();
			$line_item_args['totalvat'] = $this->number_format($order->get_total_tax(), $order);
			$line_item_args['totalsum'] = $line_item_args['totalwithvat'] - $line_item_args['totalvat'];
			$line_item_args['tax_exempt'] = $line_item_args['totalvat']==0 ? 1 : 0;
			$line_item_args['tax_rate'] = $line_item_args['tax_exempt'] ? 0 : round(10000 * $line_item_args['totalwithvat'] / $line_item_args['totalsum'])/100 - 100;
		}
		else
		{
			WC_Gateway_iCount::log("Taxes disabled!");
			$line_item_args['tax_disabled'] = 1;
		}

		return $line_item_args;
	}

	/**
	 * Get order item names as a string
	 * @param  WC_Order $order
	 * @return string
	 */
	protected function get_order_item_names( $order ) {
		$item_names = array();

		foreach ( $order->get_items() as $item ) {
			$item_names[] = $item['name'] . ' x ' . $item['qty'];
		}

		return implode( ', ', $item_names );
	}

	/**
	 * Get order item names as a string
	 * @param  WC_Order $order
	 * @param  array $item
	 * @return string
	 */
	protected function get_order_item_name( $order, $item ) {
		$item_name = $item['name'];
		$item_meta = new WC_Order_Item_Meta( $item );

		if ( $meta = $item_meta->display( true, true ) ) {
			$item_name .= ' ( ' . $meta . ' )';
		}

		return $item_name;
	}

	/**
	 * Return all line items
	 */
	protected function get_line_items() {
		return $this->line_items;
	}

	/**
	 * Remove all line items
	 */
	protected function delete_line_items() {
		$this->line_items = array();
	}

	/**
	 * Get line items to send to icount
	 *
	 * @param  WC_Order $order
	 * @return bool
	 */
	protected function prepare_line_items( $order ) {
		$this->delete_line_items();
		$calculated_total = 0;

		$wc_tax_enabled = wc_tax_enabled();
		$wc_prices_include_tax = !$wc_tax_enabled || wc_prices_include_tax();

		// Products
		foreach( $order->get_items(Array('line_item', 'fee')) as $item)
		{
			WC_Gateway_iCount::log('ITEM: ' . var_export($item,1));

			if(is_object($item))
			{
				WC_Gateway_iCount::log("Item is object");

				$item_name = $item->get_name();
				$item_type = $item->get_type();
				$item_quantity = $item->get_quantity();
				if($wc_prices_include_tax)
				{
					$item_unitprice_incvat = $order->get_item_subtotal($item, true, false);
					$item_tax = $order->get_item_tax($item, false);
					$item_unitprice = $item_unitprice_incvat - $item_tax;
				}
				else
				{
					$item_unitprice = $order->get_item_subtotal($item, false, false);
					$item_tax = $order->get_item_tax($item, false);
					$item_unitprice_incvat = $item_unitprice + $item_tax;
				}
			}
			else
			{
				WC_Gateway_iCount::log("Item is legacy array");

				$item_type = $item["type"];
				$item_name = $item["name"];
				$item_quantity = $item["qty"];
				$item_unitprice = $item["line_subtotal"]/$item_quantity;
				$item_tax = $item["line_subtotal_tax"]/$item_quantity;
				$item_unitprice_incvat = $item_unitprice + $item_tax;
			}

			if('fee' === $item_type)
			{
				$item_quantity = 1;
			}
			else
			{
				$item_name = $this->get_order_item_name($order, $item);
			}

			if(is_callable(Array($item, "get_product")))
				$product = $item->get_product();
			elseif(is_callable(Array($item, "get_product_id")))
				$product = wc_get_product($item->get_product_id());
			else
				$product = $order->get_product_from_item( $item );
			WC_Gateway_iCount::log("PRODUCT: ".var_export($product,1));

			if(is_callable(Array($product, "get_sku")))
				$item_sku = $product->get_sku();
			else
				$item_sku = null;

			WC_Gateway_iCount::log("ITEM name={$item_name} sku={$item_sku} type={$item_type} quantity={$item_quantity} unitprice={$item_unitprice} unitprice_incvat={$item_unitprice_incvat}");

			$line_item = $this->add_line_item($item_name, $item_quantity, $wc_prices_include_tax ? $item_unitprice_incvat : $item_unitprice, $item_sku, $wc_prices_include_tax);
			$calculated_total += $item_unitprice * $item_quantity;

			if(!$line_item) return false;
		}

		// Shipping Cost item - icount only allows shipping per item, we want to send shipping for the order
		$shipping_method = $order->get_shipping_method();
		if(!empty($shipping_method))
		{
			$item_line_total = $order->get_total_shipping() + $order->get_shipping_tax();
			$calculated_total += $item_line_total;

			WC_Gateway_iCount::log("Shipping Method: ".$shipping_method);
			WC_Gateway_iCount::log("Shipping Fee: ".$item_line_total);

			$this->add_line_item(
				sprintf(__('Shipping via %s', 'woocommerce'), $shipping_method),
				1,
				$this->round($item_line_total, $order),
				null,
				true // shipping never includes vat in wooco... no idea why
				);
		}

		if($discount=$order->get_total_discount(!$wc_prices_include_tax))
		{
			$item_line_total = $discount;
			$calculated_total += $item_line_total;

			$discount_desc = __("Discount","woocommerce");
			$coupon_codes = $order->get_used_coupons();
			if(!empty($coupon_codes))
			{
				$discount_desc = __("Coupon code", "woocommerce").": ".implode(", ", $coupon_codes);
			}

			WC_Gateway_iCount::log("Discount Amount: ".$discount);
			WC_Gateway_iCount::log("Discount Description: ".$discount_desc);

			$this->add_line_item(
				$discount_desc,
				1,
				-1*$discount,
				null,
				$wc_prices_include_tax
				);
		}

		return true;
	}

	/**
	 * Add iCount Line Item
	 * @param string  $item_name
	 * @param integer $quantity
	 * @param integer $amount
	 * @param string  $item_number
	 * @return bool successfully added or not
	 */
	protected function add_line_item( $item_name, $quantity = 1, $amount = 0, $item_number = '', $incvat=false ) {
		$this->line_items[] = Array(
			"description" => html_entity_decode( wc_trim_string( $item_name ? $item_name : __( 'Item', 'woocommerce' ), 250 ), ENT_NOQUOTES, 'UTF-8' ),
			"quantity" => $quantity,
			($incvat ? "unitprice_incvat" : "unitprice") => $amount,
			"sku" => $item_number,
			);
		return true;
	}

	/**
	 * Get the state to send to icount
	 * @param  string $cc
	 * @param  string $state
	 * @return string
	 */
	protected function get_icount_state( $cc, $state ) {
		if ( 'US' === $cc ) {
			return $state;
		}

		$states = WC()->countries->get_states( $cc );

		if ( isset( $states[ $state ] ) ) {
			return $states[ $state ];
		}

		return $state;
	}

	/**
	 * Check if currency has decimals
	 *
	 * @param  string $currency
	 *
	 * @return bool
	 */
	protected function currency_has_decimals( $currency ) {
		if ( in_array( $currency, array( 'HUF', 'JPY', 'TWD' ) ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Round prices
	 *
	 * @param  float|int $price
	 * @param  WC_Order $order
	 *
	 * @return float|int
	 */
	protected function round( $price, $order ) {
		$precision = 2;

		if ( ! $this->currency_has_decimals( $order->get_order_currency() ) ) {
			$precision = 0;
		}

		return round( $price, $precision );
	}

	/**
	 * Format prices
	 *
	 * @param  float|int $price
	 * @param  WC_Order $order
	 *
	 * @return float|int
	 */
	protected function number_format( $price, $order ) {
		$decimals = 2;

		if ( ! $this->currency_has_decimals( $order->get_order_currency() ) ) {
			$decimals = 0;
		}

		return number_format( $price, $decimals, '.', '' );
	}
}
