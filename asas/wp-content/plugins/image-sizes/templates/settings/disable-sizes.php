<?php
$image_sizes = $args['image_sizes'];
$selected_sizes = cx_get_option( 'prevent_image_sizes', 'disables', [] );

echo '<p class="cxis-desc">' . sprintf( __( 'You currently have <strong>%1$d thumbnails</strong> registered. It means, if you upload an image, it may generate up to %1$d duplicates of that image. Select the sizes from the list below that you want to disable from generating.', 'image-sizes' ), count( get_option( '_image-sizes' ) ) ) . '</p>';
echo '
<div id="cxis-wrap">
	<div id="check-wrap">
		<div id="check-all-wrap">
			<input type="checkbox" id="check-all" class="checkbox check-all" >
			<label for="check-all">' . __( 'All', 'image-sizes' ) . '</label>
		</div>
		<div id="check-default-wrap">
			<input type="checkbox" id="check-default" class="checkbox check-all-default" >
			<label for="check-default">' . __( 'Default', 'image-sizes' ) . '</label>
		</div>
		<div id="check-custom-wrap">
			<input type="checkbox" id="check-custom" class="checkbox check-all-custom" >
			<label for="check-custom">' . __( 'Custom', 'image-sizes' ) . '</label>
		</div>
	</div>

	<div id="sizes-counter">
		<div id="disabled-counter" class="size-counter">
			<span class="counter"></span> ' . __( 'thumbnails disabled', 'image-sizes' ) . '
		</div>
		<div id="enabled-counter" class="size-counter">
			<span class="counter"></span> ' . __( 'thumbnails will be generated', 'image-sizes' ) . '
		</div>
	</div>
</div>

<table class="form-table">
	<thead>
		<tr>
			<th>' . __( 'Enable', 'image-sizes' ) . '</th>
			<th>' . __( 'Name', 'image-sizes' ) . '</th>
			<th>' . __( 'Type', 'image-sizes' ) . '</th>
			<th>' . __( 'Width (px)', 'image-sizes' ) . '</th>
			<th>' . __( 'Height (px)', 'image-sizes' ) . '</th>
			<th>' . __( 'Cropped?', 'image-sizes' ) . '</th>
		</tr>
	</thead>
	<tbody>
		<tr id="row-main-file">
			<td><input type="checkbox" checked disabled/></td>
			<td>' . __( 'Original Image', 'image-sizes' ) . '</td>
			<td>' . __( 'original', 'image-sizes' ) . '</td>
			<td>' . __( 'auto', 'image-sizes' ) . '</td>
			<td>' . __( 'auto', 'image-sizes' ) . '</td>
			<td>' . __( 'No', 'image-sizes' ) . '</td>
		</tr>
';

if( is_array( $image_sizes ) && count( $image_sizes ) > 0 ) :
foreach ( $image_sizes as $id => $size ) {
	$_checked = in_array( $id, $selected_sizes ) ? 'checked' : '';
	$_cropped = $size['cropped'] ? __( 'Yes', 'image-sizes' ) : __( 'No', 'image-sizes' );
	echo "
		<tr id='row-{$id}'>
			<td><input type='checkbox' class='checkbox check-this check-{$size['type']}' name='disables[]' value='{$id}' {$_checked} /></td>
			<td>{$id}</td>
			<td>{$size['type']}</td>
			<td>{$size['width']}</td>
			<td>{$size['height']}</td>
			<td>{$_cropped}</td>
		</tr>";
}
endif;

echo '
	</tbody>
</table>
';