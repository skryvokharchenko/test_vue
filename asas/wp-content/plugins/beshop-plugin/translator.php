<?php

require __DIR__ . '/vendor/autoload.php';

use Google\Cloud\Translate\V3\TranslationServiceClient;

$donttranslate = false;

class Translator {

    public $translationClient;
    public $targetLanguage;
    public $targetLanguageCode;

    public function __construct() {

        if (!defined('BESHOP_AUTOTRANSLATE_TO')) {
            return;
        }
        
        $this->targetLanguage = BESHOP_AUTOTRANSLATE_TO;
        $this->targetLanguageCode = reset(explode('-', $this->targetLanguage));

        try {
            putenv('GOOGLE_APPLICATION_CREDENTIALS=' . __DIR__ . '/BeOnline-902849c79a02.json');
            $this->translationClient = new TranslationServiceClient();
        } catch (Exception $ex) {
            error_log(print_r(['INIT TRANSLATION CLIENT EXCEPTION', $ex], true));
        }


        /* HOOK WP */
        add_action('woocommerce_update_product', array($this, 'run'), 10, 1);
        add_action('woocommerce_new_product', array($this, 'run'), 10, 1);

        add_action('woocommerce_product_duplicate_before_save', array($this, 'disable_translate'), 10, 1);

//        add_action('init', array($this, 'define_cron_jobs'));
//        add_action('init', array($this, 'translate_taxonomy_terms'));
        add_action('init', array($this, 'manualy_translate_terms_and_strings'));
    }
    
    public function manualy_translate_terms_and_strings(){
//	error_log('manualy_translate_terms_and_strings');
        if($_GET['manualy_translate_terms_and_strings']==1){
//            $this->translate_strings();
//            $this->translate_terms();
//            $this->translate_taxonomy_terms();
        }
    }

    public function define_cron_jobs() {
        if (!wp_next_scheduled('translate_terms')) {
            wp_schedule_event(time(), 'daily', 'translate_terms');
        }
        add_action('translate_terms', array($this, 'translate_terms'));
        if (!wp_next_scheduled('translate_strings')) {
            wp_schedule_event(time(), 'daily', 'translate_strings');
        }
        add_action('translate_strings', array($this, 'translate_strings'));
        if (!wp_next_scheduled('translate_taxonomy_terms')) {
            wp_schedule_event(time(), 'daily', 'translate_taxonomy_terms');
        }
        add_action('translate_taxonomy_terms', array($this, 'translate_taxonomy_terms'));
    }

    public function run($product_id) {
        global $donttranslate;
        
        global $sitepress;
        
        // set russian as default lang
        $sitepress->set_default_language('ru');
        $sitepress->switch_lang('ru');

        $product = wc_get_product($product_id);

        // get num of sizes
        $sizes = wc_get_product_terms($product_id, 'pa_размер');
        $sizes_count = count($sizes);

        // get count of childrens
        $variations = get_children(array('post_type' => 'product_variation', 'post_parent' => $product_id), ARRAY_A);
        $variations_count = count($variations);

        error_log(print_r(['TRANSLATION RUN ' . $product_id . ' ' . $product->get_type(), 'SIZES' => $sizes_count, 'VARIATIONS' => $variations_count], true));

        $get_language_args = array('element_id' => $product->get_id(), 'element_type' => 'post_product');
        $original_post_language_info = apply_filters('wpml_element_language_details', null, $get_language_args);

        //check for translation exists and it is a product
        if (!icl_object_id($product_id, 'product', false, $this->targetLanguageCode) &&
                in_array($product->get_type(), ['simple', 'variable']) &&
                strpos($product->get_title(), esc_html__('Копировать', 'woocommerce')) === false &&
                $original_post_language_info->language_code != $this->targetLanguageCode &&
                !$donttranslate &&
                $sizes_count == $variations_count
        ) {
            error_log('ALL REQUIREMENTS ARE MET. TRANSLATING ');
            //Get google translation
            $translation = $this->translate([$product->get_title(), $product->get_description()]);

            $wc_adp = new WC_Admin_Duplicate_Product;
            $dproduct = $wc_adp->product_duplicate(wc_get_product($product->get_id()));

            $dproduct->set_name($translation[0]);
            $dproduct->set_description($translation[1]);
            $dproduct->set_status('publish');
            $dproduct->set_slug(sanitize_title($translation[0]));
            $dproduct->save();

            $set_language_args = array(
                'element_id' => $dproduct->get_id(),
                'element_type' => 'post_product',
                'trid' => $original_post_language_info->trid,
                'language_code' => $this->targetLanguageCode,
                'source_language_code' => 'ru'
            );
            do_action('wpml_set_element_language_details', $set_language_args);

            $this->enable_translate();

            $this->debug([$dproduct]);
        }
    }

    public function translate_strings() {
        error_log(print_r(['translate_strings START'], true));
        global $wpdb;
        $strings = $wpdb->get_results("SELECT `id`, `name`, `value` FROM `{$wpdb->prefix}icl_strings` WHERE `context` = 'WordPress' AND status = 0 and value not like 'pa_%'", ARRAY_A);
        if (!$strings || empty($strings)) {
            return;
        }
        foreach ($strings as $string) {
            $translation = $this->translate([$string['value'],'empty']);

            error_log(print_r(['STRING TRANSLATION', $string['value'], $translation[0]], true));
            $wpdb->insert($wpdb->prefix . 'icl_string_translations', array('string_id' => $string['id'], 'language' => $this->targetLanguageCode, 'status' => 10, 'value' => $translation[0]));

            icl_update_string_status($string['id']);
        }
    }

    public function translate_terms() {
        error_log(print_r(['translate_terms START'], true));
        $categories = get_terms('product_cat', ['taxonomy' => 'product_cat', 'hide_empty' => false, 'limit' => '-1']);
        foreach ($categories as $category) {
            $translationExists = icl_object_id($category->term_id, 'tax_product_cat', false, $this->targetLanguageCode);

            // Duplicate term
            $newTerm = (array) $category;

            $translation = $this->translate([$newTerm['name'],'empty']);

            if (!$translationExists && $category->name != '' && !term_exists($translation[0])) {

                error_log(print_r([$category->name, 'NO TRANSLATION'], true));

                $translatedTerm = wp_insert_term($translation[0], 'product_cat', ['slug' => sanitize_title($translation[0])]);

                $get_language_args = array('element_id' => $category->term_id, 'element_type' => 'tax_product_cat');
                $original_post_language_info = apply_filters('wpml_element_language_details', null, $get_language_args);

                $set_language_args = array(
                    'element_id' => $translatedTerm['term_id'],
                    'element_type' => 'tax_product_cat',
                    'trid' => $original_post_language_info->trid,
                    'language_code' => $this->targetLanguageCode,
                    'source_language_code' => 'ru'
                );
                do_action('wpml_set_element_language_details', $set_language_args);
            }

            // Do hierarchy sync
            $sync_helper = wpml_get_hierarchy_sync_helper('term');
            $sync_helper->sync_element_hierarchy('product_cat', $this->targetLanguageCode);
        }
    }

    public function translate_taxonomy_terms() {
        error_log(print_r(['translate_taxonomy_terms START'], true));
        
        $terms_to_translate = ['pa_категория', 'pa_сезон', 'pa_цвет'];
        $taxonomy_terms = get_terms(['taxonomy' => $terms_to_translate]);

        foreach ($taxonomy_terms as $term) {
            $translationExists = icl_object_id($term->term_taxonomy_id, $term->taxonomy, false, $this->targetLanguageCode);
            if (!$translationExists) {
                $translation = $this->translate([$term->name,'empty']);
                error_log(print_r([$term->name, 'NO TRANSLATION'], true));

                $translatedTerm = wp_insert_term($translation[0], $term->taxonomy, ['slug' => sanitize_title($translation[0])]);

                $get_language_args = array('element_id' => $term->term_taxonomy_id, 'element_type' => 'tax_' . $term->taxonomy);
                $original_post_language_info = apply_filters('wpml_element_language_details', null, $get_language_args);

                $set_language_args = array(
                    'element_id' => $translatedTerm['term_id'],
                    'element_type' => 'tax_' . $term->taxonomy,
                    'trid' => $original_post_language_info->trid,
                    'language_code' => $this->targetLanguageCode,
                    'source_language_code' => 'ru'
                );
                do_action('wpml_set_element_language_details', $set_language_args);
            }
        }

    }

    public function debug($data) {
        error_log(print_r($data, true));
//        echo '<div class="debug"><pre>';
//        echo print_r($data);
//        echo '</pre></div>';
    }

    public function translate($content) {
        try {

            $response = $this->translationClient->translateText(
                    $content, $this->targetLanguage, TranslationServiceClient::locationName('apiname-1070', 'global')
            );
        } catch (Exception $e) {
            error_log(print_r(['CALL TRANSLATE EXCEPTION', $e], true));
        }

        $translations = [];

        foreach ($response->getTranslations() as $key => $translation) {
            $separator = $key === 2 ? '!' : ' ';
            array_push($translations, $translation->getTranslatedText() . $separator);
        }
        return $translations;
    }

    public function disable_translate() {
        global $donttranslate;
        $donttranslate = true;
        error_log('DONTRANSLATE: ENABLE =======================================================');
    }

    public function enable_translate() {
        global $donttranslate;
        $donttranslate = false;
        error_log('DONTRANSLATE: DISABLE ======================================================');
    }

}

$translator = new Translator();

