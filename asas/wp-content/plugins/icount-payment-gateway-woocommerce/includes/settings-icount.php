<?php
if(!defined('ABSPATH')) exit;

/**
 * Settings for iCount Gateway
 */
return array(
	'enabled' => array(
		'title'   => __( 'Enable/Disable', 'woocommerce' ),
		'type'    => 'checkbox',
		'label'   => __( 'Enable iCount Payment Gateway', 'woocommerce' ),
		'default' => 'yes'
	),
	'title' => array(
		'title'       => __( 'Title', 'woocommerce' ),
		'type'        => 'text',
		'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
		'default'     => __( 'iCount', 'woocommerce' ),
		'desc_tip'    => true,
	),
	'description' => array(
		'title'       => __('Description', 'woocommerce'),
		'type'        => 'text',
		'desc_tip'    => true,
		'description' => __('This controls the description which the user sees during checkout.', 'woocommerce'),
		'default'     => __('Secure Checkout via iCount', 'woocommerce')
	),
	'order_button_text' => Array(
		'title'       => __('Order Button Text', 'woocommerce'),
		'type'        => 'text',
		'desc_tip'    => true,
		'description' => __('This controls the text shown to customer on the order button.', 'woocommerce'),
		'default'     => __('Proceed to iCount', 'woocommerce' ),
	),
	'environment' => array(
		'title'       => __('iCount Environment', 'woocommerce'),
		'type'        => 'select',
		'options'     => Array(
			'app'     => __('Production (latest stable)', 'wocommerce'),
			'staging' => __('Testing (bleeding edge new version, may break from time to time)', 'woocommerce'),
			),
		'default'     => 'app',
		'description' => __('Use production environment unless You are absolutely certain', 'woocommerce'),
	),
	'testmode' => array(
		'title'       => __( 'Test Mode', 'woocommerce'),
		'type'        => 'checkbox',
		'label'       => __( 'Enable Test mode', 'woocommerce'),
		'default'     => 'no',
		'description' => __('Test Mode can be used to test Checkout without actually charging the credit-card (Warning: this will still create documents).', 'woocommerce'),
	),
	'debug' => array(
		'title'       => __( 'Debug Log', 'woocommerce' ),
		'type'        => 'checkbox',
		'label'       => __( 'Enable logging', 'woocommerce' ),
		'default'     => 'no',
		'description' => sprintf(__('Log iCount events, such as IPN requests, inside <code>%s</code>', 'woocommerce'), wc_get_log_file_path('icount'))
	),
	'api_details' => array(
		'title'       => __( 'API Credentials', 'woocommerce' ),
		'type'        => 'title',
		'description' => __('Your iCount WooCommerce integration credentials. Do not change these settings unless You know what You are doing.', 'woocommerce'),
	),
	'secret' => array(
		'title'       => __( 'iCount Secret', 'woocommerce' ),
		'type'        => 'text',
		'description' => __( 'Do not change unless You know what You are doing.', 'woocommerce' ),
		'default'     => 'bedfe15271738414de4f9983fc359452',
		'placeholder' => ''
	),
	'token' => array(
		'title'       => __( 'iCount Token', 'woocommerce' ),
		'type'        => 'text',
		'description' => __( 'Do not change unless You know what You are doing.', 'woocommerce' ),
		'default'     => 'GHH6At2dnD1AGtmEGAQSIyuPxhDQugFB3dAGLq+abR9E5/XUkoxG+ICuK1Ap/HJtkFc2Y8428wh0F+2Gl6yPMA==',
		'placeholder' => ''
	),
	'disable_signature_validation' => array(
		'title'       => __( 'iCount IPN Signature', 'woocommerce' ),
		'type'        => 'checkbox',
		'label'       => __( 'Disable signature validation', 'woocommerce' ),
		'default'     => 'no',
		'description' => __('This disables verification of signature sent by iCount in IPN request. Do not change these settings unless You know what You are doing.', 'woocommerce'),
	),
	'paypal_details' => array(
		'title'       => __( 'PayPal IPN Integration', 'woocommerce' ),
		'type'        => 'title',
		'description' => __('Forward IPN request received by WooCommerce to your iCount system to produce accounting documents', 'woocommerce'),
	),
	'paypal_ipn_forward_enabled' => array(
		'title'       => __( 'PayPal IPN forward', 'woocommerce' ),
		'type'        => 'checkbox',
		'label'       => __( 'Enable PayPal IPN forwarding', 'woocommerce' ),
		'default'     => 'no',
		'description' => __( 'Forward PayPal IPN requests to iCount', 'woocommerce' ),
	),
	'paypal_ipn_forward_url' => array(
		'title'       => __( 'PayPal IPN URL', 'woocommerce' ),
		'type'        => 'text',
		'description' => __( 'PayPal IPN handler URL. See iCount system configuration for details', 'woocommerce' ),
		'default'     => '',
		'placeholder' => ''
	),
);
