<?php

/*
  Plugin Name: Beshop Plugin
  Description: Plugin that extends WP with beshop functionality
  Version:     0.0.1
  Author:      Oleksandr Palamarchuk
  Author URI:  mailto:a.palamar4uk@gmail.com
 */

defined('ABSPATH') || exit;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/manage.php';
require __DIR__ . '/backend.php';
require __DIR__ . '/translator.php';

$BeshopPlugin = new Backend();
