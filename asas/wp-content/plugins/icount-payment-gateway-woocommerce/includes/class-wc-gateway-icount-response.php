<?php
if(!defined('ABSPATH')) exit;

/**
 * Handles iCount callback requests
 */
abstract class WC_Gateway_iCount_Response extends WC_Settings_API {

	public function __construct()
	{
		$this->id = "icount";
	}

	/**
	 * Validate request signature
	 * @param Array $posted - request
	 * @return WP_Order $order - order
	 */
	protected function validate_request($posted) {
		if(empty($posted["wc_order_id"]) || empty($posted["wc_order_key"])) {
			return new WP_Error('missing_wc_order_id', "Order ID and/or key not found in request", Array("status"=>400,"status_text"=>"Bad Request"));
		}

		if(!$this->get_option("disable_signature_validation"))
		{
			if(empty($posted["wc_signature"])) {
				return new WP_Error('missing_signature', "Signature not found in the request", Array("status"=>404,"status_text"=>"Unauthorized"));
			}

			$secret = $this->get_option("secret");
			$signature = WC_Gateway_iCount::calculate_signature($posted, $secret);

			if($posted["wc_signature"] != $signature)
			{
				WC_Gateway_iCount::log("WARNING: Signature mismatch! POST[wc_signature]={$posted["wc_signature"]} calculated_signature={$signature}");
				return new WP_Error('invalid_signature', "Signature mismatch", Array("status"=>403,"status_text"=>"Forbidden"));
			}
		}

		return $this->get_response_order($posted);
	}


	/**
	 * Get the order from the IPN request
	 *
	 * @param  Array $posted - request
	 * @return bool|WC_Order object
	 */
	protected function get_response_order( $posted ) {
		$order_id  = $posted["wc_order_id"];
		$order_key = $posted["wc_order_key"];

		if ( ! $order = wc_get_order( $order_id ) ) {
			// We have an invalid $order_id, probably because invoice_prefix has changed
			$order_id = wc_get_order_id_by_order_key( $order_key );
			$order    = wc_get_order( $order_id );
		}

		if(!$order) {
			return new WP_Error('order_not_found', "Order not found", Array("status"=>404,"status_text"=>"Not Found"));
		}

		$found_order_key = is_callable(Array($order,"get_order_key")) ? $order->get_order_key() : $order->order_key;
		if ( $found_order_key != $order_key ) {
			return new WP_Error('order_key_mismatch', "Order key mismatch", Array("status"=>404,"status_text"=>"Not Found"));
		}

		return $order;
	}

	/**
	 * Complete order, add transaction ID and note
	 * @param  WC_Order $order
	 * @param  string $txn_id
	 * @param  string $note
	 */
	protected function payment_complete( $order, $txn_id = '', $note = '' ) {
		$order->add_order_note( $note );
		$order->payment_complete( $txn_id );
	}

	/**
	 * Hold order and add note
	 * @param  WC_Order $order
	 * @param  string $reason
	 */
	protected function payment_on_hold( $order, $reason = '' ) {
		$order->update_status( 'on-hold', $reason );
		$order->reduce_order_stock();
		WC()->cart->empty_cart();
	}
}
