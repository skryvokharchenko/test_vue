<?php
if(!defined('ABSPATH')) exit;

/**
 * Forward requests
 */
abstract class WC_Gateway_iCount_Forwarder extends WC_Settings_API {

	public function __construct()
	{
		$this->id = "icount";
	}

	public function forward_request($url)
	{
		if(empty($url))
		{
			WC_Gateway_iCount::log("Empty forwarder URL");
			return false;
		}

		$curl_options = Array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_TIMEOUT => 10,
			);

		// append current query string
		if(!empty($_SERVER["QUERY_STRING"]))
		{
			$url .= strpos($url,"?")!==false ? "&" : "?";
			$url .= $_SERVER["QUERY_STRING"];
		}

		WC_Gateway_iCount::log("Forwarder URL: ".$url);
		WC_Gateway_iCount::log("Forwarder METHOD: ".$_SERVER["REQUEST_METHOD"]);

		switch($_SERVER["REQUEST_METHOD"])
		{
			case "GET":
				$curl_options += Array(
					CURLOPT_HTTPGET => 1,
					);
				break;
			case "POST":
				$post_data = file_get_contents("php://input");
				WC_Gateway_iCount::log("Forwarder POST data:\n".$post_data);
				$curl_options += Array(
					CURLOPT_POST => 1,
					CURLOPT_POSTFIELDS => $post_data,
					);
				break;
			default:
				WC_Gateway_iCount::log("Not forwarding request method ".$_SERVER["REQUEST_METHOD"]);
				return false;
		}

		$ch = curl_init($url);
		curl_setopt_array($ch, $curl_options);
		$out = curl_exec($ch);
		$info = curl_getinfo($ch);
		WC_Gateway_iCount::log("Forwarder got HTTP code: ".$info["http_code"]);
		WC_Gateway_iCount::log("Forwarder got response: ".$out);
		curl_close($ch);

		return true;
	}

}
