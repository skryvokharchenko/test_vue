<?php
/**
 * iCount Payment Gateway Uninstall
 *
 * Uninstalling iCount Payment Gateway deletes all settings
 *
 * @author      iCount
 * @category    Core
 * @package     iCount-Payment-Gateway-WooCommerce/Uninstaller
 * @version     1.1
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

global $wpdb;

$wpdb->query("DELETE FROM {$wpdb->options} WHERE option_name LIKE 'woocommerce\_icount\_%';");
