<?php
if( ! function_exists( 'pri' ) ) :
function pri( $data ) {
	echo '<pre>';
	if( is_object( $data ) || is_array( $data ) ) {
		print_r( $data );
	}
	else {
		var_dump( $data );
	}
	echo '</pre>';
}
endif;

if( ! function_exists( 'cx_get_option' ) ) :
function cx_get_option( $key, $section, $default = '' ) {

	$options = get_option( $key );

	if ( isset( $options[ $section ] ) ) {
		return $options[ $section ];
	}

	return $default;
}
endif;

if( !function_exists( 'cxis_get_template' ) ) :
/**
 * Includes a template file resides in /templates diretory
 *
 * It'll look into /image-sizes directory of your active theme
 * first. if not found, default template will be used.
 * can be overriden with image-sizes_template_override_dir hook
 *
 * @param string $slug slug of template. Ex: template-slug.php
 * @param string $sub_dir sub-directory under base directory
 * @param array $fields fields of the form
 */
function cxis_get_template( $slug, $base = 'templates', $args = null ) {

	// templates can be placed in this directory
	$override_template_dir = apply_filters( 'cxis_template_override_dir', get_stylesheet_directory() . '/image-sizes/', $slug, $base, $args );
	
	// default template directory
	$plugin_template_dir = dirname( CXIS ) . "/{$base}/";

	// full path of a template file in plugin directory
	$plugin_template_path =  $plugin_template_dir . $slug . '.php';
	// full path of a template file in override directory
	$override_template_path =  $override_template_dir . $slug . '.php';

	// if template is found in override directory
	if( file_exists( $override_template_path ) ) {
		ob_start();
		include $override_template_path;
		return ob_get_clean();
	}
	// otherwise use default one
	elseif ( file_exists( $plugin_template_path ) ) {
		ob_start();
		include $plugin_template_path;
		return ob_get_clean();
	}
	else {
		return __( 'Template not found!', 'image-sizes' );
	}
}
endif;


function cxis_get_image_sizes() {
    global $_wp_additional_image_sizes;

    $thumb_crop = get_option( 'thumbnail_crop' ) == 1;

    /**
     * Standard image sizes
     */
    $sizes = [
    	'thumbnail' => [
	    	'type'		=> 'default',
	    	'width'		=> get_option( 'thumbnail_size_w' ),
	    	'height'	=> get_option( 'thumbnail_size_h' ),
	    	'cropped'	=> $thumb_crop
	    ],
	    'medium' => [
        	'type'		=> 'default',
        	'width'		=> get_option( 'medium_size_w' ),
        	'height'	=> get_option( 'medium_size_h' ),
        	'cropped'	=> $thumb_crop
        ],
        'medium_large' => [
        	'type'		=> 'default',
        	'width'		=> get_option( 'medium_large_size_w' ),
        	'height'	=> get_option( 'medium_large_size_h' ),
        	'cropped'	=> $thumb_crop
        ],
        'large' => [
        	'type'		=> 'default',
        	'width'		=> get_option( 'large_size_w' ),
        	'height'	=> get_option( 'large_size_h' ),
        	'cropped'	=> $thumb_crop
        ]
    ];

    /**
     * Additional image sizes
     */
    if( is_array( $_wp_additional_image_sizes ) && count( $_wp_additional_image_sizes ) ) :
    foreach ( $_wp_additional_image_sizes as $size => $data ) {
        $sizes[ $size ] = [
        	'type'		=> 'custom',
        	'width'		=> $data['width'],
        	'height'	=> $data['height'],
        	'cropped'	=> $data['crop'] == 1
        ];
    }
    endif;

    return $sizes;
}