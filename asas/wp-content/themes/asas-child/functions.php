<?php


function woocommerce_pre_get_posts($query) {
    if (!is_admin()) {
        $query->set('orderby', 'relevance');
    }
}

add_action('pre_get_posts', 'woocommerce_pre_get_posts', 20);

function setDefaultImage($product_id) {
    update_post_meta($product_id, "_thumbnail_id", 59731);
}

add_action('woocommerce_update_product', 'setDefaultImage', 10, 1);
add_action('woocommerce_new_product', 'setDefaultImage', 10, 1);

function override_product_image($args) {
    $id = get_the_ID();

    if (ICL_LANGUAGE_CODE == 'he') {
        $ru_product_id = apply_filters('wpml_object_id', $id, 'product', false, 'ru');
        $product = wc_get_product($ru_product_id);
    } else {
        $product = wc_get_product($id);
    }

    if ($product) {
        $meta_image = $product->get_meta('meta_main_image');

        if ($meta_image != '') {
            $image_path = parse_url($meta_image);
            $resize_path = 'https://d2bsgiz6lkfyqo.cloudfront.net/fit-in/';

            $args[0] = $resize_path . '475x365' . $image_path['path']; //$meta_image;
        }
    }


    return $args;
}

add_filter('wp_get_attachment_image_src', 'override_product_image', 10, 4);

function get_post_custom_thumb($attachment_id) {
    $id = get_the_ID();

    $product = wc_get_product($id);

    if (get_post_type($id) != 'product' || !$product) {
        return $attachment_id;
    }

    $meta_image = $product->get_meta('meta_main_image');

    if ($meta_image == '') {
        return $attachment_id;
    }

    return $meta_image;
}

add_filter('wp_get_attachment_url', function($attachment_id) {
    return get_post_custom_thumb($attachment_id);
}, 1, 1);

add_filter('image_size_names_choose', function($b = '', $img = '', $c = '') {
    return ['full' => __('Full Size')];
}, 1);

add_filter('admin_post_thumbnail_size', function($a) {
    return [];
});


add_filter('woocommerce_gallery_image_html_attachment_image_params', 'change_attachement_image_attributes', 20, 2);

function change_attachement_image_attributes($attr, $attachment) {

    $id = get_the_ID();

    $product = wc_get_product($id);
    $image = $attr['data-src'];

    $image_path = parse_url($image);
    $pices = explode('/', $image_path['path']);

    $orig_image = implode('/', ['https://antoshco-product-images.s3.eu-central-1.amazonaws.com', $pices[3], $pices[4]]);

    $image_info = getimagesize($orig_image);

    $attr['title'] = $product->get_title();
    $attr['class'] = $attr['class'] . ' b-lazy';
    $attr['data-src'] = $orig_image;
    $attr['data-large_image'] = $orig_image;
    $attr['data-large_image_width'] = $image_info[0];
    $attr['data-large_image_height'] = $image_info[1];

    return $attr;
}

function beshop_add_async_defer_attribute($tag, $handle) {
    $add_to = array('beshop-custom',
        'beshop-skip-link-focus-fix',
        'beshop-navigation',
        'beshop-material',
        'beshop-slick',
        'beshop-script',
        'beshop-search'
        );
    if (!in_array($handle, $add_to))
        return $tag;
    return str_replace(' src', ' async defer src', $tag);
}

//add_filter('script_loader_tag', 'beshop_add_async_defer_attribute', 10, 2);


?>
