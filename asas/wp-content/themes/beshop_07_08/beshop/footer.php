<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package beshop
 */

?>

	</div><!-- #content -->
	</div><!-- .wrapper -->
	<footer class="footer">
        <a href="#" class="beonline"></a>
    </footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
